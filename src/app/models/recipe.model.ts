import { Category } from './category.model';

export class Recipe {

    id: number;
    name: string;
    description: string;
    category: Category;
    userEmail: string;

    constructor(id: number, name: string, description: string, caterogy: Category, userEmail: string){

        this.name = name;
        this.description = description;
        this.id = id;
        this.category = caterogy;
        this.userEmail = userEmail;

    }


}