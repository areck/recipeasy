import { Injectable } from '@angular/core';
import { Recipe } from '../models/recipe.model';
import { Category } from '../models/category.model';
import { AuthService } from './auth.service';
import { MarthaService } from './martha.service';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { SelectControlValueAccessor } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
 

  private _recipes: Recipe[];
  private idIncrement: number;
  
  get recipes(): Observable<Recipe[]>
	{
    const currentId = this.authService.currentUser.id;
  
  return this.martha.select('list', {'userId': currentId}).pipe(
    map(recipes => recipes || []  ));
    
	}

	constructor(private martha: MarthaService, private authService: AuthService, private httpClient: HttpClient)
	{
    this._recipes =JSON.parse(localStorage.getItem('RECIPES')) || [];
		this.idIncrement = +localStorage.getItem('AI') || 0;
	}

	create(name: string, description: string, category: Category): Observable<Recipe>{

    const userId = this.authService.currentUser.id;
    return  this.martha.insert('create', { category, name, description, userId}).pipe(
      map(
        id => {
          if(id){
            return new Recipe(id, name, description, category, this.authService.currentUser.email);
          }
          else{
             return null;
          }
        }
      ) );
        /*
		this.idIncrement++;
		const newItem = new Recipe(this.idIncrement, name, description, category, this.authService.currentUser.email);
    this._recipes.push(newItem);
    
    localStorage.setItem('RECIPES', JSON.stringify(this._recipes));
		localStorage.setItem('AI', `${this.idIncrement}`);
*/
  }

  private getUrl(query: string)
  {
    return `http://martha.jh.shawinigan.info/queries/${query}/execute`;
  }

  get(id: number): Observable<Recipe>
	{
    const currentId = id;
		return this.martha.select('read', {'id': currentId}).pipe(
      map(recipes => recipes[0] || []  )) ;
  }
  
  update(name: string, description: string, category: Category, recipe: Recipe): Observable<boolean>{
    
    const id = recipe.id;
    return  this.martha.update('update', { category, name, description, id}).pipe(
      map(success => {
        return success;
      }
      )
    );
    
  }
  
  
  delete(recipe: Recipe): Observable<boolean> {

    console.log('hello');
    return this.martha.delete('delete', {'id': recipe.id})
/*
    this._items = this._items.filter(i => i.id !== item.id);

    localStorage.setItem('ITEMS', JSON.stringify(this._items));*/
  }
  



}
