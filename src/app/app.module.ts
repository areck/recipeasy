import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { routes } from './app.routes';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecipesComponent } from './components/recipes/recipes.component';
import { AuthService } from './services/auth.service';
import { NavComponent } from './components/nav/nav.component';
import { AuthGuardService } from './services/auth-guard.service';
import { RecipeCreateComponent } from './components/recipe-create/recipe-create.component';
import { RecipesService } from './services/recipes.service';
import { RecipeRowComponent } from './components/recipe-row/recipe-row.component';
import { RecipeDetailComponent } from './components/recipe-detail/recipe-detail.component';
import {  HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http'
import { MarthaService } from './services/martha.service';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http);
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    RecipesComponent,
    NavComponent,
    RecipeCreateComponent,
    RecipeRowComponent,
    RecipeDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
          useFactory: HttpLoaderFactory, // exported factory function needed for AoT compilation
          deps: [HttpClient]
      }
    }),
  ],
  providers: [AuthService, AuthGuardService, RecipesService, MarthaService,TranslateService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
