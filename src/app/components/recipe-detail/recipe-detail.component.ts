import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipesService } from 'src/app/services/recipes.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  @Output() readonly recipeView: EventEmitter<Recipe> = new EventEmitter();

  recipe: Recipe = null;

  recipeForm: FormGroup;
  nameControl: FormControl;
  descriptionControl: FormControl;
  categoryControl: FormControl;
  id: number;

  constructor(formBuilder: FormBuilder, route:ActivatedRoute, private recipesService: RecipesService, private router: Router) {


    this.nameControl = new FormControl('', Validators.required);
    this.descriptionControl = new FormControl('', Validators.required);
    this.categoryControl = new FormControl('', Validators.required);
    this.recipeForm = formBuilder.group(
      {
        name: this.nameControl,
        description: this.descriptionControl,
        category: this.categoryControl
      }
        );

    route.paramMap.subscribe(params => {
      this.id = Number(params.get('id'));
      this.recipesService.get(this.id).subscribe(recipe =>{ 
        this.recipe = recipe  ;
      if(this.recipe == null)
      {
        this.router.navigate(['/recipes']);
      }
      

      this.recipeForm.controls.name.setValue(this.recipe.name);
      this.recipeForm.controls.description.setValue(this.recipe.description);      
      this.recipeForm.controls.category.setValue(this.recipe.category);

      
      
  });
  
   
       
			
       } );

       

   }

  ngOnInit() {
  }

  update()
  {
    if (this.recipeForm.valid) {
      const recipesValues = this.recipeForm.value;
      console.log(recipesValues.name);
      this.recipesService.update(recipesValues.name, recipesValues.description, recipesValues.category, this.recipe).subscribe(success => {
        this.router.navigate(['/recipes']);
      }
      );
     

    }
  }

}
