import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { RecipesComponent } from '../recipes/recipes.component';
import { RecipesService } from 'src/app/services/recipes.service';
import { Recipe } from 'src/app/models/recipe.model';

@Component({
  selector: 'app-recipe-create',
  templateUrl: './recipe-create.component.html',
  styleUrls: ['./recipe-create.component.css']
})
export class RecipeCreateComponent implements OnInit {

  recipeForm: FormGroup;
  nameControl: FormControl;
  descriptionControl: FormControl;
  categoryControl: FormControl;

  @Output() readonly recipeCreate: EventEmitter<Recipe> = new EventEmitter();

  @ViewChild('form', {static: true}) formElement: NgForm;


  constructor(formBuilder: FormBuilder, private recipesServices: RecipesService) { 
    this.nameControl = new FormControl('', Validators.required);
        this.descriptionControl = new FormControl('', Validators.required);
        this.categoryControl = new FormControl('', Validators.required);
				this.recipeForm = formBuilder.group(
					{
						name: this.nameControl,
            description: this.descriptionControl,
            category: this.categoryControl
					}
				);
  }

  ngOnInit() {
  }
  
  create(){
    if (this.recipeForm.valid) {
      const recipesValues = this.recipeForm.value;
      this.recipesServices.create(recipesValues.name, recipesValues.description, recipesValues.category).subscribe(

        newRecipe => {

          if (newRecipe)
          {
            this.recipeCreate.emit(newRecipe);
          }
          else
          {
            alert('Could not complete create !')
          }
        }
      );

      this.formElement.resetForm();
  }
  }
  
}
