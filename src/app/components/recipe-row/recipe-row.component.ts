import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipesService } from 'src/app/services/recipes.service';
import { Router } from '@angular/router';

@Component({
  selector: '[app-recipe-row]',
  templateUrl: './recipe-row.component.html',
  styleUrls: ['./recipe-row.component.css']
})
export class RecipeRowComponent implements OnInit {

  @Input() recipe: Recipe;
  @Output() recipeDelete: EventEmitter<any> = new EventEmitter();   

  constructor(private recipesService: RecipesService, private router: Router) {


   }

   delete()
   {
      this.recipesService.delete(this.recipe).subscribe(success => this.recipeDelete.emit());
   }

   view()
   {
    this.router.navigate(['/recipes/', this.recipe.id]);
   }
   

  ngOnInit() {
  }


}
