import { Component, OnInit } from '@angular/core';
import { RecipesService } from 'src/app/services/recipes.service';
import { Recipe } from 'src/app/models/recipe.model';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {

  private recipes: Recipe[]= [];

  constructor(private recipesService: RecipesService) { 
    recipesService.recipes.subscribe(recipes => this.recipes = recipes);
  }

  ngOnInit() {
  }

  recipeCreated(newRecipe: Recipe)
{
  this.recipes.push(newRecipe);
}

  recipeDeleted()
  {
    this.recipesService.recipes.subscribe(recipes => this.recipes = recipes);
  }

  
  

}
