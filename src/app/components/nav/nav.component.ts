import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, public translate: TranslateService) { 
  

  }

  ngOnInit() {

    if(localStorage.getItem('lang'))
    {
    this.translate.setDefaultLang(localStorage.getItem('lang'));
    }
    else
    {
        this.translate.setDefaultLang('en');
    }
  }

  get loggedIn(): boolean {
    return this.authService.currentUser != null;
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/']);
  }

  language(lang: string)
  {
    this.translate.use(lang);

    localStorage.setItem('lang', lang);
  }
 

}
